#!/bin/bash

curl https://cernbox.cern.ch/index.php/s/gP9JKza7TxMYqmG/download --output dataset.tar
mkdir dataset/
tar -xvf dataset.tar -C ./dataset/
cd ./dataset
tar -xzvf B32.tar.xz
unzip IB03.zip
unzip IB06.zip