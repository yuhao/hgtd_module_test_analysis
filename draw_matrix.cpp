#include "TGraph.h"
#include "TCanvas.h"
#include "TGaxis.h"
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include "TROOT.h"
using namespace std;
#include "TH2F.h"
#include "TStyle.h"

TCanvas* DefineSquarePlot (const char* name,int w, float l, float r, float b, float t);


void draw_matrix(){
    TCanvas* c1=DefineSquarePlot("c1",800,0.1,0.15,0.1,0.15);
    TH2F* tf=new TH2F("TOA","TOA",5,0,5,5,0,5);
    Double_t toa[25]={21.4,21.8,21.2,22.1,23.3,21.1,21.7,22,21.5,22.4,22.2,21.9,22,0,21.6,0,21.6,22.9,22.1,23.6,22.3,23,0,0,0};
    Double_t tot[25]={157,158,159,161,166,158,160,161,160,166,159,0,161,162,167,160,162,163,162,168,163,163,162,164,167};
    for(int i=0;i<25;i++){
        int nrow=int((24-i)/5);
        int ncol=(24-i)%5;
        tf->Fill(ncol,nrow,toa[i]);
    }
    gStyle->SetOptStat(0);
    tf->SetMinimum(15);
    tf->SetMaximum(25);
    tf->GetXaxis()->SetLabelSize(0);
    tf->GetYaxis()->SetLabelSize(0);
    tf->GetXaxis()->SetNdivisions(5);
    tf->GetYaxis()->SetNdivisions(5);
    tf->GetXaxis()->SetTickLength(0);
    tf->GetYaxis()->SetTickLength(0);
    gStyle->SetTitleY(0.92);
    gStyle->SetTitleSize(0.05);
    tf->SetMarkerSize(2);
    tf->Draw("colzTEXT");
}


TCanvas* DefineSquarePlot (const char* name,int w, float l, float r, float b, float t)
{
   int h = ((1.-(l+r))*w)/(1.-(b+t));
   TCanvas* C = new TCanvas(name,name,w,h);
   C->SetLeftMargin(l),
   C->SetRightMargin(r),
   C->SetBottomMargin(b),
   C->SetTopMargin(t);
   return C;
}