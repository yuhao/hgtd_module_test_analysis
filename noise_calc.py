from ROOT import *
import numpy as np
from array import array
import pandas as pd

filepwd="/Users/yuhaowang/Documents/QT/Test0823/IB09/THRES_NEW/thres_B_9_rin_0_0_toabusy_0_rtest_ch_0_cd_0_delay_2450_Q_5_vthc_64_thres_-1__1629895107.txt"
data=pd.read_csv(filepwd,sep=" ",skiprows=2,header=None)
datalist=(data.T).values.tolist()

thres=array('d',datalist[0])
HitCnt=array('d',datalist[2])

muMin=np.min(thres)
muMax=np.max(thres)
erf =TF1("erf","[0]*(1+TMath::Erf(-(x-[1])/(sqrt(2)*[2])))",muMin,muMax)
erf.SetParameters(50,200,2)
tg=TGraph(100,thres,HitCnt)
tg.Fit(erf,"MQ")

print(erf.GetParameter(2))