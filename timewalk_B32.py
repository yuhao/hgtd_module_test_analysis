import pandas as pd
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import os
import pathlib
import numpy as np
from scipy import interpolate

def read_tw(filepwd):
    file = open(filepwd,'r')

    lines=file.readlines()
    TOTc=[0]*63
    TOTf=[0]*63
    TOAa=[0]*63


    for i in range(0,len(lines)):
        line=lines[i].rstrip("\n")
        splitline=line.split(",")
        if(splitline[0].find("NofIterations")>=0 or splitline[0].find("Parameter")>=0):
            continue
        Qvalue=int(splitline[0])
        num_nonzero=0
        avg=0
        for j in range(2,len(splitline)):
            value=float(splitline[j])
            if value==127 or value==0:
                continue
            avg+=value
            num_nonzero+=1
        if(num_nonzero!=0):avg=avg/num_nonzero
        if(splitline[1]=="HitDataTOA"):
            TOAa[Qvalue]=avg
        if(splitline[1]=="HitDataTOTc"):
            TOTc[Qvalue]=avg
        if(splitline[1]=="HitDataTOTf"):
            TOTf[Qvalue]=avg
    return TOAa,TOTc,TOTf
Qlist=range(63)

def read_all(foldername,drawHV,channel):
    measure_list=[]
    for wirefolder in os.listdir(foldername):
        if(wirefolder.endswith("wires")):
            wirenumber=wirefolder[0:wirefolder.find("wires")]
            num=int(wirenumber)
            wirepath=pathlib.Path(os.path.join(foldername,wirefolder))
            scanfile=wirepath.glob("*"+drawHV+"_*/TW*_ch_"+channel+"*.data")
            if(num==0):
                scanfile=wirepath.glob("**/"+"TW*_ch_"+channel+"*.data")
            for item in scanfile:
                # print(item)
                TOAa,TOTc,TOTf=read_tw(item)
                tup=(num,TOAa,TOTc,TOTf)
                measure_list.append(tup)
                # if(num!=0):
                #     ax.scatter(Qlist,TOTc,color=cmap(int(wirenumber)),alpha=0.7,label=wirenumber)
                # else:
                #     ax.scatter(Qlist,TOTc,color=cmap(int(wirenumber)),alpha=1,label=wirenumber,marker='^')
    return measure_list
foldername="dataset/B32"
channel="9"
drawHV="70V"
draw_item=6 #0=TOT vs Q,1=TOA vs TOT,2=TOT/TOT_0wires vs Q ,3=TOTslope vs Q, 4=TOTc_interp vs Q, 5=TOA v.s. TOT interp, 6=TOA v.s TOT slope
wires_to_draw=[0,1,21,31]#Empty tuple will draw all the wires

fig,ax=plt.subplots(figsize=(8,6))
plt.text(0.4,0.9,"B32 channel "+channel,fontsize=15,weight='bold', style='italic',transform=ax.transAxes)
plt.text(0.4,0.84,"Bias Voltage: 70V",fontsize=15,transform=ax.transAxes)
ymax=128
plt.ylim(0,ymax)

cmap = cm.get_cmap("turbo", 32)

measure_list=read_all(foldername,drawHV,channel)
measure_list.sort()
TOTc0=measure_list[0][2]
for measurement in measure_list:
    wire_no=measurement[0]
    TOAa=measurement[1]
    TOTc=measurement[2]
    TOTf=measurement[3]
    x_fordraw=[]
    y_fordraw=[]
    y_interp=[]
    if((wire_no not in wires_to_draw) and len(wires_to_draw)!=0):
        continue
    if(draw_item==0):
        x_fordraw=Qlist
        y_fordraw=TOTc
    if(draw_item==1):
        x_fordraw=TOTc
        y_fordraw=TOAa
    if(draw_item==2):
        x_fordraw=Qlist
        y_fordraw=np.array(TOTc)/np.array(TOTc0)
    if(draw_item==3):
        tck = interpolate.splrep(Qlist, TOTc,s=100)
        y_interp=interpolate.splev(Qlist, tck)
        yder = interpolate.splev(Qlist, tck, der=1)
        x_fordraw=Qlist
        y_fordraw=yder
    if(draw_item==4):
        tck = interpolate.splrep(Qlist, TOTc,s=100)
        y_interp=interpolate.splev(Qlist, tck)
        yder = interpolate.splev(Qlist, tck, der=1)
        x_fordraw=Qlist
        y_fordraw=TOTc
    if(draw_item==5):
        sortT=list(zip(TOTc[7:], TOAa[7:]))
        sortT.sort(key=lambda x:x[0])
        # print(np.array(sortT)[:,0])
        tck = interpolate.splrep(np.array(sortT)[:,0], np.array(sortT)[:,1],s=10)
        y_interp=interpolate.splev(np.array(sortT)[:,0],tck)
        x_fordraw=np.array(sortT)[:,0]
        y_fordraw=np.array(sortT)[:,1]
        # print(y_interp)
    if(draw_item==6):
        sortT=list(zip(TOTc[7:], TOAa[7:]))
        sortT.sort(key=lambda x:x[0])
        # print(np.array(sortT)[:,0])
        tck = interpolate.splrep(np.array(sortT)[:,0], np.array(sortT)[:,1],s=10)
        y_interp=interpolate.splev(np.array(sortT)[:,0],tck)
        yder = interpolate.splev(np.array(sortT)[:,0], tck, der=1)
        x_fordraw=np.array(sortT)[:,0]
        y_fordraw=yder
    if(wire_no!=0):
        ax.scatter(x_fordraw,y_fordraw,color=cmap(wire_no),alpha=0.7,label=str(wire_no))
        if(draw_item==4):
            ax.plot(Qlist,y_interp,color=cmap(wire_no),alpha=0.7)
        if(draw_item==5):
            ax.plot(x_fordraw,y_interp,color=cmap(wire_no),alpha=0.7)

        # ax.scatter(TOTc,TOAa,color=cmap(wire_no),alpha=0.7,label=str(wire_no))
    else:
        if(draw_item!=2):
            ax.scatter(x_fordraw,y_fordraw,color=cmap(wire_no),alpha=1,label=str(wire_no),marker='^')
            if(draw_item==4):
                ax.plot(Qlist,y_interp,color=cmap(wire_no),alpha=1)
            if(draw_item==5):
                ax.plot(x_fordraw,y_interp,color=cmap(wire_no),alpha=0.7)
        else:
            plt.axhline(y=1,color=cmap(wire_no),alpha=1,label=str(wire_no))
        # ax.scatter(TOTc,TOAa,color=cmap(wire_no),alpha=1,label=str(wire_no),marker='^')
#Drawing 0 wires again to cover
# ax.scatter(Qlist,measure_list[0][2],color=cmap(0),alpha=1,marker='^')
# ax.scatter(measure_list[0][2],measure_list[0][1],color=cmap(0),alpha=1,marker='^')
# wireno_list.sort()
# print(wireno_list)
# for i in range(0,len(wireno_list)):
#     ax.text(0.1+0.05*(i%5),0.85-0.05*int(i/5),str(wireno_list[i]),transform=ax.transAxes,color=cmap(wireno_list[i]))


# plt.xlabel("TOTc")
# plt.ylabel("TOA")
legend=plt.legend(loc='upper left',ncol=4,handletextpad=0.01,columnspacing=0.3)
frame = legend.get_frame() 
frame.set_alpha(1) 
frame.set_facecolor('white')
frame.set_edgecolor('white')
if(draw_item==0 or draw_item==4):
    plt.xlabel("Q [fC]")
    plt.ylabel("TOTc")
if(draw_item==1 or draw_item==5):
    plt.xlabel("TOTc")
    plt.ylabel("TOA")
if(draw_item==2):
    plt.xlabel("Q [fC]")
    plt.ylabel(r'$\frac{TOTc}{TOTc_{0wire}}$')
    frame.set_edgecolor('black')
    plt.ylim(0,2)
if(draw_item==3):
    plt.xlabel("Q [fC]")
    plt.ylabel('TOTc_slope')
    plt.ylim(-1,3)
if(draw_item==6):
    plt.xlabel("TOTc")
    plt.ylabel('TOAa_slope')
    plt.ylim(-1,5)
plt.show()
        
