import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def read_tw(filepwd):
    file = open(filepwd,'r')

    lines=file.readlines()
    TOTc=[0]*63
    TOTf=[0]*63
    TOAa=[0]*63


    for i in range(0,len(lines)):
        line=lines[i].rstrip("\n")
        splitline=line.split(",")
        if(splitline[0].find("NofIterations")>=0 or splitline[0].find("Parameter")>=0):
            continue
        Qvalue=int(splitline[0])
        num_nonzero=0
        avg=0
        for j in range(2,len(splitline)):
            value=float(splitline[j])
            if value==127 or value==0:
                continue
            avg+=value
            num_nonzero+=1
        if(num_nonzero!=0):avg=avg/num_nonzero
        if(splitline[1].find("HitDataTOA")==0):
            TOAa[Qvalue]=avg
        if(splitline[1].find("HitDataTOTc")==0):
            TOTc[Qvalue]=avg
        if(splitline[1].find("HitDataTOTf")==0):
            TOTf[Qvalue]=avg
    return TOAa,TOTc,TOTf
Qlist=range(63)

TOAa31,TOTc31,TOTf31=read_tw("/Users/yuhaowang/Documents/QT/B32/31wires/B32_TW_70V_1229/TW_B_32_rin_0_toabusy_1_rtest_0_ch_9_cd_0_delay_2500_thres_280_vthc_64_.data")
TOAa21,TOTc21,TOTf21=read_tw("/Users/yuhaowang/Documents/QT/B32/21wires/B21_21wires_70V_TW/TW_B_32_rin_0_toabusy_1_rtest_0_ch_9_cd_0_delay_2500_thres_280_vthc_64_.data")
TOAa0,TOTc0,TOTf0=read_tw("/Users/yuhaowang/Documents/QT/B32/0wires/B32_0wires_TW/TW_B_32_rin_0_toabusy_1_rtest_0_ch_9_cd_0_delay_2500_thres_280_vthc_64_.data")
TOAa1,TOTc1,TOTf1=read_tw("/Users/yuhaowang/Documents/QT/B32/1wires/B32_1wires_70V_TW/TW_B_32_rin_0_toabusy_1_rtest_0_ch_9_cd_0_delay_2500_thres_280_vthc_64__1641187655.data")


# TOAa31,TOTc31,TOTf31=read_tw("/Users/yuhaowang/Documents/QT/B32/31wires/B32_TW_70V_1229/TW_B_32_rin_0_toabusy_1_rtest_0_ch_10_cd_0_delay_2500_thres_306_vthc_64_.data")
# TOAa21,TOTc21,TOTf21=read_tw("/Users/yuhaowang/Documents/QT/B32/21wires/B21_21wires_70V_TW/TW_B_32_rin_0_toabusy_1_rtest_0_ch_10_cd_0_delay_2500_thres_306_vthc_64_.data")
# TOAa0,TOTc0,TOTf0=read_tw("/Users/yuhaowang/Documents/QT/B32/0wires/B32_0wires_TW/TW_B_32_rin_0_toabusy_1_rtest_0_ch_10_cd_0_delay_2500_thres_306_vthc_64_.data")
# TOAa1,TOTc1,TOTf1=read_tw("/Users/yuhaowang/Documents/QT/B32/1wires/B32_1wires_70V_TW/TW_B_32_rin_0_toabusy_1_rtest_0_ch_10_cd_0_delay_2500_thres_306_vthc_64__1641187697.data")
cmap = cm.get_cmap("turbo", 32)
fig,ax=plt.subplots(figsize=(8,6))
plt.ylim(0,128)
plt.scatter(Qlist,TOTc0,color=cmap(0),alpha=1,label="0 wire",marker='^')
plt.scatter(Qlist,TOTc1,color=cmap(1),alpha=0.7,label="1 wire -70V")
plt.scatter(Qlist,TOTc21,color=cmap(21),alpha=0.7,label="21wires-70V")
plt.scatter(Qlist,TOTc31,color=cmap(31),alpha=0.7,label="31wires-70V")
plt.text(0.4,0.9,"B32 channel 9",fontsize=15,weight='bold', style='italic',transform=ax.transAxes)


plt.xlabel("Q [fC]")
plt.ylabel("TOTc")
plt.legend(loc="upper left")
plt.show()