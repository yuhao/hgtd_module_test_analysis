# hgtd_module_test_analysis

# Updates on 2022.1.15:
Updates the pyROOT version of plots and the C macro of some plots

# Initial Commit on 2022.1.14
This folder consists the code for the analysis of HGTD ALTIROC1 module test results.

’sh download_testdata.sh’ to get the dataset used in the code

draw_matrix.cpp will draw a square hist plot showing the TOA/TOT test results 

noise_calc.py will calculate the noise of single channel with the threshold scan results

timewalk_B**.py can draw various kind of plots, showing the timing resolution of the module. The drawing item can be selected with the variable: draw_item

The test code can be found at: https://github.com/NikolaMakovec/atlas-altiroc-daq/tree/TestBenchNikola
