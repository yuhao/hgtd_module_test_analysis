import pandas as pd
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import os
import pathlib

def read_tw(filepwd):
    file = open(filepwd,'r')

    lines=file.readlines()
    TOTc=[0]*63
    TOTf=[0]*63
    TOAa=[0]*63


    for i in range(0,len(lines)):
        line=lines[i].rstrip("\n")
        splitline=line.split(",")
        if(splitline[0].find("NofIterations")>=0 or splitline[0].find("Parameter")>=0):
            continue
        Qvalue=int(splitline[0])
        num_nonzero=0
        avg=0
        for j in range(2,len(splitline)):
            value=float(splitline[j])
            if value==127 or value==0:
                continue
            avg+=value
            num_nonzero+=1
        if(num_nonzero!=0):avg=avg/num_nonzero
        if(splitline[1].find("HitDataTOA")==0):
            TOAa[Qvalue]=avg
        if(splitline[1].find("HitDataTOTc")==0):
            TOTc[Qvalue]=avg
        if(splitline[1].find("HitDataTOTf")==0):
            TOTf[Qvalue]=avg
    return TOAa,TOTc,TOTf
Qlist=range(63)


foldername="dataset/IB06"
channel="10"
drawHV="70V"
fig,ax=plt.subplots(figsize=(8,6))
plt.text(0.4,0.9,"IB06 channel "+channel,fontsize=15,weight='bold', style='italic',transform=ax.transAxes)
plt.text(0.4,0.84,"Bias Voltage: 70V",fontsize=15,transform=ax.transAxes)
plt.ylim(0,128)
cmap = cm.get_cmap("turbo", 20)
measure_list=[]

for wirefolder in os.listdir(foldername):
    if(wirefolder.endswith("wires")):
        wirenumber=wirefolder[0:wirefolder.find("wires")]
        num=int(wirenumber)
        wirepath=pathlib.Path(os.path.join(foldername,wirefolder))
        scanfile=wirepath.glob("*"+drawHV+"_TW/TW*_ch_"+channel+"*.data")
        if(num==0):
            scanfile=wirepath.glob("**/"+"TW*_ch_"+channel+"*.data")
        for item in scanfile:
            # print(item)
            TOAa,TOTc,TOTf=read_tw(item)
            tup=(num,TOAa,TOTc,TOTf)
            measure_list.append(tup)
            # if(num!=0):
            #     ax.scatter(Qlist,TOTc,color=cmap(int(wirenumber)),alpha=0.7,label=wirenumber)
            # else:
            #     ax.scatter(Qlist,TOTc,color=cmap(int(wirenumber)),alpha=1,label=wirenumber,marker='^')

       
measure_list.sort()
print(measure_list[0][0])
for measurement in measure_list:
    wire_no=measurement[0]
    TOAa=measurement[1]
    TOTc=measurement[2]
    TOTf=measurement[3]
    if(wire_no!=0):
        ax.scatter(Qlist,TOTc,color=cmap(wire_no),alpha=0.7,label=str(wire_no))
    else:
        ax.scatter(Qlist,TOTc,color=cmap(wire_no),alpha=1,label=str(wire_no),marker='^')
#Drawing 0 wires again to cover
ax.scatter(Qlist,measure_list[0][2],color=cmap(0),alpha=1,marker='^')
# wireno_list.sort()
# print(wireno_list)
# for i in range(0,len(wireno_list)):
#     ax.text(0.1+0.05*(i%5),0.85-0.05*int(i/5),str(wireno_list[i]),transform=ax.transAxes,color=cmap(wireno_list[i]))
plt.xlabel("Q [fC]")
plt.ylabel("TOTc")
plt.legend(loc='upper left',frameon=False,ncol=4,handletextpad=0.01,columnspacing=0.3)
plt.show()
        
