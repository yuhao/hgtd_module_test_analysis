from re import L
import pandas as pd
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import os
import pathlib
import numpy as np
from scipy import interpolate
# from ROOT import TCanvas, TGraph, TLatex
from ROOT import *
from array import array

def read_tw(filepwd):
    file = open(filepwd,'r')

    lines=file.readlines()
    TOTc=[0]*63
    TOTf=[0]*63
    TOAa=[0]*63


    for i in range(0,len(lines)):
        line=lines[i].rstrip("\n")
        splitline=line.split(",")
        if(splitline[0].find("NofIterations")>=0 or splitline[0].find("Parameter")>=0):
            continue
        Qvalue=int(splitline[0])
        num_nonzero=0
        avg=0
        for j in range(2,len(splitline)):
            value=float(splitline[j])
            if value==127 or value==0:
                continue
            avg+=value
            num_nonzero+=1
        if(num_nonzero!=0):avg=avg/num_nonzero
        if(splitline[1]=="HitDataTOA"):
            TOAa[Qvalue]=avg
        if(splitline[1]=="HitDataTOTc"):
            TOTc[Qvalue]=avg
        if(splitline[1]=="HitDataTOTf"):
            TOTf[Qvalue]=avg
    return TOAa,TOTc,TOTf
Qlist=range(63)

def read_all(foldername,drawHV,channel):
    measure_list=[]
    for wirefolder in os.listdir(foldername):
        if(wirefolder.endswith("wires")):
            wirenumber=wirefolder[0:wirefolder.find("wires")]
            num=int(wirenumber)
            wirepath=pathlib.Path(os.path.join(foldername,wirefolder))
            scanfile=wirepath.glob("*"+drawHV+"_*/TW*_ch_"+channel+"*.data")
            if(num==0):
                scanfile=wirepath.glob("**/"+"TW*_ch_"+channel+"*.data")
            for item in scanfile:
                # print(item)
                TOAa,TOTc,TOTf=read_tw(item)
                tup=(num,TOAa,TOTc,TOTf)
                measure_list.append(tup)
                # if(num!=0):
                #     ax.scatter(Qlist,TOTc,color=cmap(int(wirenumber)),alpha=0.7,label=wirenumber)
                # else:
                #     ax.scatter(Qlist,TOTc,color=cmap(int(wirenumber)),alpha=1,label=wirenumber,marker='^')
    return measure_list
foldername="dataset/B32"
channel="9"
drawHV="70V"
draw_item=6 #0=TOT vs Q,1=TOA vs TOT,2=TOT/TOT_0wires vs Q ,3=TOTslope vs Q, 4=TOTc_interp vs Q, 5=TOA v.s. TOT interp, 6=TOA v.s TOT slope
wires_to_draw=[0,1,21,31]#Empty tuple will draw all the wires

# fig,ax=plt.subplots(figsize=(8,6))
# plt.text(0.4,0.9,"B32 channel "+channel,fontsize=15,weight='bold', style='italic',transform=ax.transAxes)
# plt.text(0.4,0.84,"Bias Voltage: 70V",fontsize=15,transform=ax.transAxes)
# ymax=128
# plt.ylim(0,ymax)
c1 = TCanvas("c","c",1200,800)
l=TLatex()
l.SetNDC()

h_axes = TH1F("","", 100, 0, 128)
h_axes.Fill(-10, 1000)
h_axes.GetYaxis().SetTitle("TOTc")
h_axes.GetXaxis().SetTitle("Q [fC]")
h_axes.GetYaxis().SetRangeUser(0,128)
# gPad.DrawFrame(0,0,64,128)
if(draw_item==0):
    h_axes.GetXaxis().SetRangeUser(0,64)
    h_axes.GetYaxis().SetTitle("TOA")
    h_axes.GetXaxis().SetTitle("TOTc")
if(draw_item==1):
    h_axes.GetYaxis().SetTitle("TOA")
    h_axes.GetXaxis().SetTitle("TOTc")
if(draw_item==2):
    h_axes.GetYaxis().SetRangeUser(0,2)
    h_axes.GetXaxis().SetRangeUser(0,64)
    h_axes.GetYaxis().SetTitle("#frac{TOTc}{TOTc_{0wire}}")
    h_axes.GetXaxis().SetTitle("Q [fC]")
if(draw_item==3):
    h_axes.GetXaxis().SetRangeUser(0,64)
    h_axes.GetYaxis().SetRangeUser(-1,3)
    h_axes.GetYaxis().SetTitle("TOTslope")
    h_axes.GetXaxis().SetTitle("Q [fC]")
if(draw_item==4):
    h_axes.GetXaxis().SetRangeUser(0,64)
    h_axes.GetYaxis().SetRangeUser(0,128)
    h_axes.GetYaxis().SetTitle("TOTc_interp")
    h_axes.GetXaxis().SetTitle("Q [fC]")
if(draw_item==5):
    h_axes.GetYaxis().SetTitle("TOA_interp")
    h_axes.GetXaxis().SetTitle("TOTc")
if(draw_item==6):
    h_axes.GetYaxis().SetRangeUser(-1,3)
    h_axes.GetYaxis().SetTitle("TOA_slope")
    h_axes.GetXaxis().SetTitle("TOTc")
# h_axes.GetYaxis().SetLabelOffset(99)
# h_axes.GetXaxis().SetLabelOffset(99)   
h_axes.SetLineColor(0)
h_axes.Draw("HISTSame")
gStyle.SetOptTitle(0)
gStyle.SetOptStat(0)

graph_list=[None]*64
cmap = cm.get_cmap("turbo", 32)
measure_list=read_all(foldername,drawHV,channel)
measure_list.sort()
TOTc0=measure_list[0][2]
for measurement in measure_list:
    wire_no=measurement[0]
    TOAa=measurement[1]
    TOTc=measurement[2]
    TOTf=measurement[3]
    x_fordraw=[]
    y_fordraw=[]
    y_interp=[]
    if((wire_no not in wires_to_draw) and len(wires_to_draw)!=0):
        continue
    if(draw_item==0):
        x_fordraw=Qlist
        y_fordraw=TOTc
    if(draw_item==1):
        x_fordraw=TOTc
        y_fordraw=TOAa
    if(draw_item==2):
        x_fordraw=Qlist
        y_fordraw=np.array(TOTc)/np.array(TOTc0)
    if(draw_item==3):
        tck = interpolate.splrep(Qlist, TOTc,s=100)
        y_interp=interpolate.splev(Qlist, tck)
        yder = interpolate.splev(Qlist, tck, der=1)
        x_fordraw=Qlist
        y_fordraw=yder
    if(draw_item==4):
        tck = interpolate.splrep(Qlist, TOTc,s=100)
        y_interp=interpolate.splev(Qlist, tck)
        yder = interpolate.splev(Qlist, tck, der=1)
        x_fordraw=Qlist
        y_fordraw=TOTc
    if(draw_item==5):
        sortT=list(zip(TOTc[7:], TOAa[7:]))
        sortT.sort(key=lambda x:x[0])
        # print(np.array(sortT)[:,0])
        tck = interpolate.splrep(np.array(sortT)[:,0], np.array(sortT)[:,1],s=10)
        y_interp=interpolate.splev(np.array(sortT)[:,0],tck)
        x_fordraw=np.array(sortT)[:,0]
        y_fordraw=np.array(sortT)[:,1]
        # print(y_interp)
    if(draw_item==6):
        sortT=list(zip(TOTc[7:], TOAa[7:]))
        sortT.sort(key=lambda x:x[0])
        # print(np.array(sortT)[:,0])
        tck = interpolate.splrep(np.array(sortT)[:,0], np.array(sortT)[:,1],s=10)
        y_interp=interpolate.splev(np.array(sortT)[:,0],tck)
        yder = interpolate.splev(np.array(sortT)[:,0], tck, der=1)
        x_fordraw=np.array(sortT)[:,0]
        y_fordraw=yder
    graph_list[wire_no]=TGraph()
    colorpy=cmap(wire_no)
    color=TColor.GetColor(colorpy[0],colorpy[1],colorpy[2])
    if(wire_no!=0):
        for i in range(0,len(x_fordraw)):
           graph_list[wire_no].SetPoint(i,x_fordraw[i],y_fordraw[i])
        graph_list[wire_no].SetMarkerColorAlpha(color,0.7)
        graph_list[wire_no].SetMarkerStyle(20)
        graph_list[wire_no].SetMarkerSize(1.5)
        graph_list[wire_no].Draw('P,SAME')
        graph_list[wire_no].SetLineWidth(0)
        if(draw_item==4 or draw_item==5):
            graph_list[wire_no].SetLineColor(color)
            graph_list[wire_no].SetLineWidth(1)
            graph_list[wire_no].Draw('CP,SAME')
    else:
        if(draw_item!=2):
            for i in range(0,len(x_fordraw)):
                graph_list[wire_no].SetPoint(i,x_fordraw[i],y_fordraw[i])
            graph_list[wire_no].SetMarkerColor(color)
            graph_list[wire_no].SetMarkerStyle(22)
            graph_list[wire_no].SetMarkerSize(1.5)
            graph_list[wire_no].Draw('P,SAME')
            graph_list[wire_no].SetLineWidth(0)
            if(draw_item==4 or draw_item==5):
                graph_list[wire_no].SetLineColor(color)
                graph_list[wire_no].SetLineWidth(1)
                graph_list[wire_no].Draw('CP,SAME')
        else:
            graph_list[wire_no].SetPoint(0,0,1)
            graph_list[wire_no].SetPoint(1,128,1)
            graph_list[wire_no].SetLineColor(color)
            graph_list[wire_no].SetLineWidth(1)
            graph_list[wire_no].Draw('CSAME')

if((0 in wires_to_draw) or len(wires_to_draw)==0):
    if(draw_item!=2):
        graph_list[0].Draw("PSame")
        if(draw_item==4 or draw_item==5):
            graph_list[0].Draw('CP,SAME')
    else:graph_list[0].Draw('CSAME')

l.DrawLatex(0.4,0.81,"#font[72]{B32 channel "+channel+"}")
if(drawHV=="70V"):
    l.DrawLatex(0.4,0.73,"#font[42]{Bias Voltage: 70V}")
if(drawHV=="noHV"):
    l.DrawLatex(0.4,0.73,"#font[42]{Bias Voltage: 0V}")
if(drawHV=="150V"):
    l.DrawLatex(0.4,0.73,"#font[42]{Bias Voltage: 150V}")


lg=TLegend(0.15,0.7,0.35,0.89)
lg.SetFillColor(kWhite)
lg.SetBorderSize(1)
lg.SetNColumns(5)
for i in range(0,len(graph_list)):
    graph=graph_list[i]
    if (graph!=None):
        lg.AddEntry(graph,str(i))
lg.Draw("Same")
filepwd="plot/B32_ch"+channel+"_"+drawHV+"_"+str(draw_item)
# c1.Show()
c1.SaveAs(filepwd+".C")
c1.SaveAs(filepwd+".pdf")